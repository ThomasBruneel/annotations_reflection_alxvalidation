package com.ardis.entities;

import com.ardis.AlxAnnotation;

public class Adress {
    @AlxAnnotation(condition = "a",resetValue = "abc")
    public String streetName;

    @AlxAnnotation(condition = "a",resetValue = "0")
    public int zipCode;

    public Adress(String streetName, int zipCode) {
        this.streetName = streetName;
        this.zipCode = zipCode;
    }
}
