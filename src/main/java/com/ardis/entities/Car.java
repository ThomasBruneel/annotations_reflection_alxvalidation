package com.ardis.entities;

import com.ardis.AlxAnnotation;

public class Car {

    @AlxAnnotation(condition = "a || b",resetValue = "4")
    public int numberOfWheels;
    @AlxAnnotation(condition = "a || b",resetValue = "empty")
    public String brand;

    public Car(int numberOfWheels, String brand) {
        this.numberOfWheels = numberOfWheels;
        this.brand = brand;
    }
}
