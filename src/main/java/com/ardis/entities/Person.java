package com.ardis.entities;

import com.ardis.AlxAnnotation;

import java.util.ArrayList;

public class Person {

    @AlxAnnotation(condition = "a || b",resetValue = "abc")
    public String firstName;

    @AlxAnnotation(condition = "a || b",resetValue = "0")
    public int age;

    public Adress adress;

    public ArrayList<Car>cars;

    @AlxAnnotation(condition = "a || b",resetValue = "0")
    public Dimension distance;

    @AlxAnnotation(condition = "a || b",resetValue = "0")
    public ArrayList<Integer>points;

    @AlxAnnotation(condition = "a || b",resetValue = "false")
    public boolean hasChildren;

    @AlxAnnotation(condition = "a || b",resetValue = "FEMALE")
    public GenderEnum gender;

    @AlxAnnotation(condition = "a || b",resetValue = "RED")
    public HairColorEnum hairColor;


    public Person(String firstName, int age,Adress adress,ArrayList<Car>cars, ArrayList<Integer> points,Dimension distance,boolean hasChildren,GenderEnum gender,HairColorEnum hairColor) {
        this.firstName = firstName;
        this.age = age;
        this.adress = adress;
        this.points = points;
        this.distance=distance;
        this.hasChildren=hasChildren;
        this.gender=gender;
        this.hairColor=hairColor;
        this.cars=cars;
    }

    public Person(Dimension dimension) {
        this.distance=dimension;
    }

    public Person(int age) {
        this.age=age;
    }
}
