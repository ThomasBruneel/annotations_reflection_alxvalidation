package com.ardis.entities;

import java.util.Objects;

public class Dimension {

    public String value;

    public Dimension(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dimension dimension = (Dimension) o;
        return Objects.equals(value, dimension.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
