package com.ardis;

import com.ardis.entities.*;
import org.apache.commons.beanutils.ConvertUtils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) throws IllegalAccessException {
	// write your code here
        Person p1 =new Person("thomas",
                0,
                new Adress("straat",9000),
                new ArrayList<Car>(Arrays.asList(new Car(5,"opel"),new Car(6,"Opel2"),new Car(4,"Opel3"))),
                new ArrayList<>(Arrays.asList(1,2,3,4)),
                new Dimension("0"),
                true,
                GenderEnum.MALE,
                HairColorEnum.BLONDE);
        ArrayList<Field>list=new ArrayList<>();
        recursiveReflection(p1,list);
        System.out.println("total fields: "+list.size());

        list= (ArrayList<Field>) list
                .stream()
                .filter(f->f.getAnnotation(AlxAnnotation.class) instanceof AlxAnnotation)
                .collect(Collectors.toList());

        System.out.println("total fields with alx annotation: "+list.size());

    }



    private static void recursiveReflection(Object parent, ArrayList<Field> list) throws IllegalAccessException {
            Field[] fields=parent.getClass().getDeclaredFields();
            for(Field f:fields){
                Object o=f.get(parent);
                if(o!=null){
                    list.add(f);
                    //check if field is an collection, map  or array
                    Object[] children=null;
                    if (o instanceof Collection)
                        children = ((Collection)o).toArray();
                    else if (o instanceof Map)
                        children = ((Map)o).values().toArray();
                    else if (o instanceof Object[])
                        children = (Object[])o;
                    //1. if children is not null then it is an collection, map or array
                    if(children!=null){
                        for(Object child:children){
                            //if child is not primitive type AND not not java object (Integer, Boolean) AND not dimension class, don't go deeper in object
                            // go only deeper when it is a custom object
                            if(!child.getClass().isPrimitive() && !isJavaLang(child) && child.getClass() != Dimension.class){
                                recursiveReflection(child, list);
                            }
                        }
                    }
                    //2. if object is primitive, java object or custom object
                    else{
                        //if object is not primitive type AND not not java object (Integer, Boolean) AND not dimension class, don't go deeper in object
                        // go only deeper when it is a custom object
                        if(!o.getClass().isPrimitive() && !isJavaLang(o) && o.getClass() != Dimension.class && !(o instanceof Enum<?>) ){
                            recursiveReflection(o, list);
                        }
                    }
                    //if field is annotated with AlxAnnotation do check
                    if(f.getAnnotation(AlxAnnotation.class) instanceof AlxAnnotation){
                        checkField(f,parent);
                    }

                }


            }
    }

    private static void checkField(Field f, Object parent) throws IllegalAccessException {
        Object fieldValue=f.get(parent);
        String resetValue=f.getAnnotation(AlxAnnotation.class).resetValue();

        if (fieldValue.getClass().isPrimitive()||isJavaLang(fieldValue)||fieldValue.getClass()==String.class) {
            if(!fieldValue.equals(ConvertUtils.convert(resetValue,fieldValue.getClass()))){
                System.out.println(String.format("updated field %s from %s to %s ",f.getName(),fieldValue,resetValue));
                f.set(parent, ConvertUtils.convert(resetValue,fieldValue.getClass()));
            }

        }
        else if(fieldValue.getClass()==Dimension.class){
            if(!new Dimension(resetValue).equals(fieldValue)){
                System.out.println(String.format("updated field %s from %s to %s ",f.getName(),((Dimension) fieldValue).value,resetValue));
                f.set(parent,new Dimension(resetValue));
            }
        }
        else if(fieldValue instanceof Enum<?>){
            if(!((Enum)fieldValue).equals(resetValue)){
                System.out.println(String.format("updated field %s from %s to %s ",f.getName(),fieldValue,resetValue));
                f.set(parent,getEnumFromString(resetValue,(Class)fieldValue.getClass()));
            }
        }
        else if(fieldValue instanceof Collection || fieldValue instanceof Object[]){
            f.set(parent,null);
        }


    }


    public static boolean isJavaLang(Object check) {
        return check.getClass().getName().startsWith("java.lang");
    }

    public static <T extends Enum<T>> T getEnumFromString(String string, Class<T> c) {
        if( c != null && string != null ) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            } catch(IllegalArgumentException ex) {
            }
        }
        return null;
    }

}
